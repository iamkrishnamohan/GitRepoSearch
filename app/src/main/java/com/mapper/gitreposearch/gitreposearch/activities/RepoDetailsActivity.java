package com.mapper.gitreposearch.gitreposearch.activities;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mapper.gitreposearch.gitreposearch.R;
import com.mapper.gitreposearch.gitreposearch.adapters.ContributorsListAdapter;
import com.mapper.gitreposearch.gitreposearch.adapters.SearchRepositoryAdapter;
import com.mapper.gitreposearch.gitreposearch.common.CustomCallBack;
import com.mapper.gitreposearch.gitreposearch.common.GridSpacingItemDecoration;
import com.mapper.gitreposearch.gitreposearch.common.ServiceGenerator;
import com.mapper.gitreposearch.gitreposearch.interfaces.ApiService;
import com.mapper.gitreposearch.gitreposearch.model.ContributorBean;
import com.mapper.gitreposearch.gitreposearch.model.ContributorsModel;
import com.mapper.gitreposearch.gitreposearch.model.Items;
import com.mapper.gitreposearch.gitreposearch.model.SearchRepositoryModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

public class RepoDetailsActivity extends AppCompatActivity {

    private static final String TAG = RepoDetailsActivity.class.getSimpleName();
    private ArrayList<Items> detailsList;
    private ApiService mService;

    @BindView(R.id.recyclerViewContributors)
    RecyclerView recyclerViewContributor;

    @BindView(R.id.imageViewRepo)
    ImageView imageRepo;

    @BindView(R.id.tvDetailName)
    TextView detailName;

    @BindView(R.id.webView)
    WebView webView;

    @BindView(R.id.tvDetailProjectLink)
    TextView detailProjectLink;

    @BindView(R.id.tvDetailDescription)
    TextView detailDescription;
    private String valueReceivedName, valueReceivedProjectLink, valueReceivedDescription, ValueReceivedImageUrl, valueReceivedContributorsURL;
    private List<ContributorBean> contributorsList;
    private ContributorsListAdapter contributorsListAdapter;
    private LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repo_details);
        mService = ServiceGenerator.GetBaseUrl(ApiService.class);
        ButterKnife.bind(this);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            valueReceivedName = bundle.getString("Name");
            valueReceivedProjectLink = bundle.getString("ProjectLink");
            valueReceivedDescription = bundle.getString("Description");
            ValueReceivedImageUrl = bundle.getString("Image");
            valueReceivedContributorsURL = bundle.getString("Contributors");
        }
        Log.e(TAG, "onCreate: URL " + valueReceivedContributorsURL);
        contributorsList = new ArrayList<>();
        /*if (intent!=null) {
            String detailName= intent.
            List<Items> detailsList = (ArrayList<Items>) intent.getSerializableExtra("searchList");
        }
        Intent intent = getIntent();
        if (intent != null)
             detailsList = (ArrayList<Items>) intent.getSerializableExtra("searchList");
*/

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 3);
        recyclerViewContributor.setLayoutManager(mLayoutManager);
        recyclerViewContributor.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerViewContributor.setItemAnimator(new DefaultItemAnimator());
        //recyclerViewContributor.setAdapter(adapter);

        detailName.setText(valueReceivedName);
        detailProjectLink.setText(valueReceivedProjectLink);
        detailDescription.setText(valueReceivedDescription);
        if (ValueReceivedImageUrl != null) {
            Glide.with(this).load(ValueReceivedImageUrl)
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageRepo);
        }

        detailProjectLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                webView.setVisibility(View.VISIBLE);
                webView.getSettings().setJavaScriptEnabled(true);
                webView.loadUrl(valueReceivedProjectLink);
                webView.setHorizontalScrollBarEnabled(false);
            }
        });
        if (!valueReceivedContributorsURL.isEmpty())
            getAllContributors(valueReceivedContributorsURL);

    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    private void getAllContributors(String valueReceivedContributorsURL) {


// Initialize a new RequestQueue instance
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        // Initialize a new JsonArrayRequest instance
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET,
                valueReceivedContributorsURL,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        // Do something with response
                        //mTextView.setText(response.toString());

                        // Process the JSON
                        try {
                            // Loop through the array elements
                            for (int i = 0; i < response.length(); i++) {
                                // Get current json object
                                JSONObject contributor = response.getJSONObject(i);
                                ContributorBean contributorBean = new ContributorBean();
                                // Get the current contributor (json object) data
                                String contributorName = contributor.getString("login");
                                String contributorAvatar = contributor.getString("avatar_url");
                                String contributorRepoUrl = contributor.getString("repos_url");
                                contributorBean.setContributorName(contributorName);
                                contributorBean.setGetContributorAvatar(contributorAvatar);
                                contributorBean.setContributorRepoUrl(contributorRepoUrl);
                                contributorsList.add(contributorBean);

                            }

                            if (contributorsListAdapter == null) {
                                contributorsListAdapter = new ContributorsListAdapter(RepoDetailsActivity.this, contributorsList);
                                recyclerViewContributor.setAdapter(contributorsListAdapter);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Do something when error occurred
                        Toast.makeText(RepoDetailsActivity.this, "Error..." + error, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        // Add JsonArrayRequest to the RequestQueue
        requestQueue.add(jsonArrayRequest);
    }

}
