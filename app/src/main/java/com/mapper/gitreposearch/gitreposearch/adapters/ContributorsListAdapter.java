package com.mapper.gitreposearch.gitreposearch.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mapper.gitreposearch.gitreposearch.R;
import com.mapper.gitreposearch.gitreposearch.activities.ContributorDetailsActivity;
import com.mapper.gitreposearch.gitreposearch.activities.RepoDetailsActivity;
import com.mapper.gitreposearch.gitreposearch.model.ContributorBean;
import com.mapper.gitreposearch.gitreposearch.model.ContributorsModel;

import java.util.List;

/**
 * Created by user on 2/23/2018.
 */

public class ContributorsListAdapter extends RecyclerView.Adapter<ContributorsListAdapter.MyViewHolder> {

    private Context mContext;
    private List<ContributorBean> contributorsList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView thumbnail;
        public RelativeLayout contributorListItem;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            contributorListItem = (RelativeLayout) view.findViewById(R.id.contributorListItem);
        }
    }


    public ContributorsListAdapter(Context mContext, List<ContributorBean> contributorsList) {
        this.mContext = mContext;
        this.contributorsList = contributorsList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contributors_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final ContributorBean contributor = contributorsList.get(position);
        holder.title.setText(contributor.getContributorName());
        // loading album cover using Glide library
        Glide.with(mContext).load(contributor.getGetContributorAvatar()).into(holder.thumbnail);
        holder.contributorListItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ContributorDetailsActivity.class);
               intent.putExtra("ContributorRepoURL", contributor.getContributorRepoUrl());
               intent.putExtra("ContributorImage",contributor.getGetContributorAvatar());
               intent.putExtra("ContributorName",contributor.getContributorName());

                mContext.startActivity(intent);
            }
        });

    }


    @Override
    public int getItemCount() {
        return contributorsList.size();
    }
}
