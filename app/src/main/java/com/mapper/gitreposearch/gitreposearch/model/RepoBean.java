package com.mapper.gitreposearch.gitreposearch.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by user on 2/23/2018.
 */

public class RepoBean implements Parcelable{

    private String repoName;
    private String repoUrl;
    private String repoAvatarUrl;
    private String repoProjectUrl;
    private String repoDescription;
    private String repoContributor;

    public RepoBean() {
    }

    protected RepoBean(Parcel in) {
        repoName = in.readString();
        repoUrl = in.readString();
        repoAvatarUrl = in.readString();
        repoProjectUrl = in.readString();
        repoDescription = in.readString();
        repoContributor = in.readString();
    }

    public static final Creator<RepoBean> CREATOR = new Creator<RepoBean>() {
        @Override
        public RepoBean createFromParcel(Parcel in) {
            return new RepoBean(in);
        }

        @Override
        public RepoBean[] newArray(int size) {
            return new RepoBean[size];
        }
    };

    public String getRepoName() {
        return repoName;
    }

    public void setRepoName(String repoName) {
        this.repoName = repoName;
    }

    public String getRepoUrl() {
        return repoUrl;
    }

    public void setRepoUrl(String repoUrl) {
        this.repoUrl = repoUrl;
    }

    public String getRepoAvatarUrl() {
        return repoAvatarUrl;
    }

    public void setRepoAvatarUrl(String repoAvatarUrl) {
        this.repoAvatarUrl = repoAvatarUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getRepoProjectUrl() {
        return repoProjectUrl;
    }

    public void setRepoProjectUrl(String repoProjectUrl) {
        this.repoProjectUrl = repoProjectUrl;
    }

    public String getRepoDescription() {
        return repoDescription;
    }

    public void setRepoDescription(String repoDescription) {
        this.repoDescription = repoDescription;
    }

    public String getRepoContributor() {
        return repoContributor;
    }

    public void setRepoContributor(String repoContributor) {
        this.repoContributor = repoContributor;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(repoName);
        parcel.writeString(repoUrl);
        parcel.writeString(repoAvatarUrl);
        parcel.writeString(repoProjectUrl);
        parcel.writeString(repoDescription);
        parcel.writeString(repoContributor);
    }
}
