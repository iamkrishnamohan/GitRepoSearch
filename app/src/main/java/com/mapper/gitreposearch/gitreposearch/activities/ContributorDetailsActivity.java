package com.mapper.gitreposearch.gitreposearch.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mapper.gitreposearch.gitreposearch.R;
import com.mapper.gitreposearch.gitreposearch.adapters.ContributorReposListAdapter;
import com.mapper.gitreposearch.gitreposearch.adapters.ContributorsListAdapter;
import com.mapper.gitreposearch.gitreposearch.interfaces.ApiService;
import com.mapper.gitreposearch.gitreposearch.model.ContributorBean;
import com.mapper.gitreposearch.gitreposearch.model.Items;
import com.mapper.gitreposearch.gitreposearch.model.RepoBean;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContributorDetailsActivity extends AppCompatActivity {
    private static final String TAG = ContributorDetailsActivity.class.getSimpleName();


    @BindView(R.id.recyclerViewRepoList)
    RecyclerView recyclerViewRepoList;

    @BindView(R.id.imageViewContributor)
    ImageView imageContributor;

    @BindView(R.id.tvContributorName)
    TextView tvContributorName;
    private String contributorImageUrl, contributorName, contributorRepoUrl;
    private List<RepoBean> reposList;

    private LinearLayoutManager linearLayoutManager;
    private ContributorReposListAdapter contributorReposListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contributor_details);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        if (intent != null) {
            contributorName = intent.getStringExtra("ContributorName");
            contributorImageUrl = intent.getStringExtra("ContributorImage");
            contributorRepoUrl = intent.getStringExtra("ContributorRepoURL");
        }
        reposList = new ArrayList<>();
        tvContributorName.setText(contributorName);
        if (contributorImageUrl != null) {
            Glide.with(this).load(contributorImageUrl)
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageContributor);
        }
        recyclerViewRepoList.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(ContributorDetailsActivity.this);
        recyclerViewRepoList.setLayoutManager(linearLayoutManager);

        if (!contributorRepoUrl.isEmpty())
            getAllContributorsRepoList(contributorRepoUrl);

        Log.e(TAG, "onCreate: Repos " + contributorRepoUrl);
    }

    private void getAllContributorsRepoList(String contributorRepoUrl) {

        // Initialize a new RequestQueue instance
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        // Initialize a new JsonArrayRequest instance
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET,
                contributorRepoUrl,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        // Do something with response
                        //mTextView.setText(response.toString());

                        // Process the JSON
                        try {
                            // Loop through the array elements
                            for (int i = 0; i < response.length(); i++) {
                                // Get current json object
                                JSONObject repos = response.getJSONObject(i);
                                JSONObject ownerObject = repos.getJSONObject("owner");
                                RepoBean repoBean = new RepoBean();
                                // Get the current contributor (json object) data
                                String repoName = ownerObject.getString("login");
                                String repoAvatar = ownerObject.getString("avatar_url");
                                String repoUrl = ownerObject.getString("repos_url");
                                String repoProjectLinkUrl = ownerObject.getString("html_url");
                                String repoContributors = repos.getString("contributors_url");
                                repoBean.setRepoName(repoName);
                                repoBean.setRepoAvatarUrl(repoAvatar);
                                repoBean.setRepoUrl(repoUrl);
                                repoBean.setRepoProjectUrl(repoProjectLinkUrl);
                                repoBean.setRepoContributor(repoContributors);
                                reposList.add(repoBean);

                            }

                            if (contributorReposListAdapter == null) {
                                contributorReposListAdapter = new ContributorReposListAdapter(ContributorDetailsActivity.this, reposList);
                                recyclerViewRepoList.setAdapter(contributorReposListAdapter);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Do something when error occurred
                        Toast.makeText(ContributorDetailsActivity.this, "Error..." + error, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        // Add JsonArrayRequest to the RequestQueue
        requestQueue.add(jsonArrayRequest);
    }
}
