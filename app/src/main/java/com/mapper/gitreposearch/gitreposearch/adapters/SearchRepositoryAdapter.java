package com.mapper.gitreposearch.gitreposearch.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mapper.gitreposearch.gitreposearch.R;
import com.mapper.gitreposearch.gitreposearch.activities.RepoDetailsActivity;
import com.mapper.gitreposearch.gitreposearch.model.Items;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 2/23/2018.
 */

public class SearchRepositoryAdapter extends RecyclerView.Adapter<SearchRepositoryAdapter.SearchViewHolder> {

    private Context mContext;
    private List<Items> mSearchList;


    public SearchRepositoryAdapter(Context context, List<Items> list) {
        this.mContext = context;
        this.mSearchList = list;

    }


    @Override
    public SearchRepositoryAdapter.SearchViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(
                R.layout.search_list_item, null);

        return new SearchRepositoryAdapter.SearchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SearchRepositoryAdapter.SearchViewHolder holder, final int position) {

        holder.searchName.setText(mSearchList.get(position).getName());
        holder.searchFullName.setText(mSearchList.get(position).getFull_name());
        holder.viewCommit.setVisibility(View.VISIBLE);
        holder.tvWatcherCount.setText(mSearchList.get(position).getWatchers_count());
        holder.tvForkCount.setText(mSearchList.get(position).getForks_count());

        Glide.with(mContext).load(mSearchList.get(position).getOwner().getAvatar_url())
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.imgPic);

       holder.leadsListItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Bundle bundle = new Bundle();
               // b.putParcelableArrayList("searchList",  mSearchList);

                bundle.putString("Name", mSearchList.get(position).getOwner().getLogin());
                bundle.putString("ProjectLink", mSearchList.get(position).getHtml_url());
                bundle.putString("Description",mSearchList.get(position).getDescription());
                bundle.putString("Image",mSearchList.get(position).getOwner().getAvatar_url());
                bundle.putString("Contributors",mSearchList.get(position).getContributors_url());
               Intent intent = new Intent(mContext, RepoDetailsActivity.class);
                //intent.putExtra("searchList", (ArrayList<Items>) mSearchList.get(position));
                intent.putExtras(bundle);
                mContext.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {

        //return mSearchList.size();
        return 10;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    public class SearchViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView searchName, searchFullName, tvWatcherCount, tvForkCount;
        private ImageView imgPic;
        private RelativeLayout leadsListItem;
        private LinearLayout viewCommit;


        public SearchViewHolder(View itemView) {
            super(itemView);
            searchName = itemView.findViewById(R.id.tvSearchName);
            leadsListItem = itemView.findViewById(R.id.searchListItem);
            searchFullName = itemView.findViewById(R.id.tvSearchFullName);
            tvWatcherCount = itemView.findViewById(R.id.tvWatcherCount);
            tvForkCount = itemView.findViewById(R.id.tvForkCount);
            imgPic = itemView.findViewById(R.id.imageView);
            viewCommit=itemView.findViewById(R.id.viewCommit);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

        }
    }
}
