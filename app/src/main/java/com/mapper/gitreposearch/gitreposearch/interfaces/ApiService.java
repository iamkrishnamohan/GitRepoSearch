package com.mapper.gitreposearch.gitreposearch.interfaces;

import com.mapper.gitreposearch.gitreposearch.common.UrlUtils;
import com.mapper.gitreposearch.gitreposearch.model.ContributorsModel;
import com.mapper.gitreposearch.gitreposearch.model.Items;
import com.mapper.gitreposearch.gitreposearch.model.SearchRepositoryModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by user on 2/22/2018.
 */

public interface ApiService {

    @GET(UrlUtils.GET_USER_SEARCH_RESULTS)
    Call<SearchRepositoryModel> getSearchResults(@Query("q") String  searchResult,
                                                         @Query("sort") String sort,
                                                         @Query("order") String order);


    @GET
    Call<List<ContributorsModel>> getContributors(@Url String url);

}
