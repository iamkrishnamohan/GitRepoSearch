package com.mapper.gitreposearch.gitreposearch.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by user on 2/22/2018.
 */

public class SearchRepositoryModel implements Parcelable{


    @Expose
    @SerializedName("items")
    private List<Items> items;
    @Expose
    @SerializedName("incomplete_results")
    private boolean incomplete_results;
    @Expose
    @SerializedName("total_count")
    private String total_count;

    protected SearchRepositoryModel(Parcel in) {
        incomplete_results = in.readByte() != 0;
        total_count = in.readString();
    }

    public static final Creator<SearchRepositoryModel> CREATOR = new Creator<SearchRepositoryModel>() {
        @Override
        public SearchRepositoryModel createFromParcel(Parcel in) {
            return new SearchRepositoryModel(in);
        }

        @Override
        public SearchRepositoryModel[] newArray(int size) {
            return new SearchRepositoryModel[size];
        }
    };

    public List<Items> getItems() {
        return items;
    }

    public void setItems(List<Items> items) {
        this.items = items;
    }

    public boolean getIncomplete_results() {
        return incomplete_results;
    }

    public void setIncomplete_results(boolean incomplete_results) {
        this.incomplete_results = incomplete_results;
    }

    public String getTotal_count() {
        return total_count;
    }

    public void setTotal_count(String total_count) {
        this.total_count = total_count;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte((byte) (incomplete_results ? 1 : 0));
        parcel.writeString(total_count);
    }
}
