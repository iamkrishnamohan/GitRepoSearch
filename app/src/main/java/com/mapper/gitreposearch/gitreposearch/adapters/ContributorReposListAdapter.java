package com.mapper.gitreposearch.gitreposearch.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mapper.gitreposearch.gitreposearch.R;
import com.mapper.gitreposearch.gitreposearch.activities.RepoDetailsActivity;
import com.mapper.gitreposearch.gitreposearch.model.Items;
import com.mapper.gitreposearch.gitreposearch.model.RepoBean;

import java.util.List;

/**
 * Created by user on 2/23/2018.
 */

public class ContributorReposListAdapter extends RecyclerView.Adapter<ContributorReposListAdapter.ViewHolder> {

    private Context mContext;
    private List<RepoBean> mRepoList;


    public ContributorReposListAdapter(Context context, List<RepoBean> list) {
        this.mContext = context;
        this.mRepoList = list;

    }


    @Override
    public ContributorReposListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(
                R.layout.search_list_item, null);

        return new ContributorReposListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ContributorReposListAdapter.ViewHolder holder, final int position) {

        holder.searchName.setText(mRepoList.get(position).getRepoName());
        holder.searchFullName.setText(mRepoList.get(position).getRepoUrl());


        Glide.with(mContext).load(mRepoList.get(position).getRepoAvatarUrl())
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.imgPic);

        holder.leadsListItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                // b.putParcelableArrayList("searchList",  mSearchList);

                bundle.putString("Name", mRepoList.get(position).getRepoName());
                bundle.putString("ProjectLink", mRepoList.get(position).getRepoProjectUrl());
                bundle.putString("Description",mRepoList.get(position).getRepoDescription());
                bundle.putString("Image",mRepoList.get(position).getRepoAvatarUrl());
                bundle.putString("Contributors",mRepoList.get(position).getRepoContributor());
                Intent intent = new Intent(mContext, RepoDetailsActivity.class);
                intent.putExtras(bundle);
                mContext.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {

        //return mSearchList.size();
        return mRepoList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView searchName, searchFullName;
        private ImageView imgPic;
        private RelativeLayout leadsListItem;


        public ViewHolder(View itemView) {
            super(itemView);
            searchName = itemView.findViewById(R.id.tvSearchName);
            leadsListItem = itemView.findViewById(R.id.searchListItem);
            searchFullName = itemView.findViewById(R.id.tvSearchFullName);

            imgPic = itemView.findViewById(R.id.imageView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

        }
    }
}