package com.mapper.gitreposearch.gitreposearch.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;

import com.mapper.gitreposearch.gitreposearch.R;
import com.mapper.gitreposearch.gitreposearch.adapters.SearchRepositoryAdapter;
import com.mapper.gitreposearch.gitreposearch.common.CustomCallBack;
import com.mapper.gitreposearch.gitreposearch.common.ServiceGenerator;
import com.mapper.gitreposearch.gitreposearch.interfaces.ApiService;
import com.mapper.gitreposearch.gitreposearch.model.Items;
import com.mapper.gitreposearch.gitreposearch.model.SearchRepositoryModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private AutoCompleteTextView autoCompleteTextView;
    private SearchView search;
    MenuItem itemSearch;
    private int startIndex;
    private boolean userScrolled;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private LinearLayoutManager linearLayoutManager;
    private boolean isFetching;
    private List<Items> searchList;
    private ApiService mService;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;


    @BindView(R.id.txtNoData)
    TextView txtNoData;
    private SearchRepositoryAdapter searchRepositoryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mService = ServiceGenerator.GetBaseUrl(ApiService.class);
        ButterKnife.bind(this);

        searchList = new ArrayList<>();
        if(searchList.isEmpty()){
            txtNoData.setVisibility(View.VISIBLE);
            txtNoData.setText(R.string.no_data_text);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);


        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        search = (SearchView) menu.findItem(R.id.action_search).getActionView();
        search.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
        search.setQueryHint("Search Repository");
        search.setIconified(true);

        recyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);

        AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView) search.findViewById(R.id.search_src_text);
        autoCompleteTextView.setDropDownBackgroundResource(R.color.transparent);
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.e(TAG, "onQueryTextSubmit: "+query );
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(search.getWindowToken(), 0);
                return true;
            }

            @Override
            public boolean onQueryTextChange(final String query) {
                if (query.length() > 3)
                    fetchSearchResults(query.toLowerCase());
                return true;
            }

        });
        return true;
    }

    private void fetchSearchResults(final String searchString) {

        startIndex = 0;
        initializeData(startIndex, searchString);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    userScrolled = true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                if (userScrolled
                        && (visibleItemCount + pastVisiblesItems) == totalItemCount
                        && !isFetching) {

                    userScrolled = false;
                    startIndex = searchList.size() + 1;
                    initializeData(startIndex, searchString);
                    isFetching = true;
                }
            }
        });
    }

    private void initializeData(final int index, final String searchString) {
        recyclerView.setVisibility(View.VISIBLE);
        txtNoData.setVisibility(View.GONE);

        Call<SearchRepositoryModel> call = mService.getSearchResults(searchString, "stars", "desc");
        call.enqueue(new CustomCallBack<SearchRepositoryModel>(this) {
            @Override
            public void onRequestSuccess(SearchRepositoryModel response) {
                searchList.addAll(response.getItems());
                if (searchRepositoryAdapter == null) {
                    searchRepositoryAdapter = new SearchRepositoryAdapter(MainActivity.this, searchList);
                    recyclerView.setAdapter(searchRepositoryAdapter);
                }

            }
        });
    }
}
