package com.mapper.gitreposearch.gitreposearch.common;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.mapper.gitreposearch.gitreposearch.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Custom callback method for retrofit apis
 *
 * @param <T>
 */
public abstract class CustomCallBack<T> implements Callback<T> {

    private String TAG = CustomCallBack.class.getSimpleName();
    private Context mContext;
    private ProgressDialog progressDialog;

    /**
     * To show progress bar
     *
     * @param context passing context
     */
    public CustomCallBack(Context context) {
        mContext = context;
        showProgressBar(context);
    }

    /**
     * Hiding progress bar
     * checking and handling null
     *
     * @param call
     * @param response
     */
    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        hideProgressBar();
        if (response != null && response.isSuccessful()) {
            if (response.body() == null) {
                Toast.makeText(mContext, mContext.getResources().getString(R.string.error), Toast.LENGTH_SHORT).show();
                return;
            } else {
                onRequestSuccess(response.body());
            }
        } else {
            Toast.makeText(mContext, mContext.getResources().getString(R.string.error), Toast.LENGTH_SHORT).show();
            return;
        }
    }

    /**
     * showing failure method
     *
     * @param call
     * @param t
     */
    @Override
    public void onFailure(Call<T> call, Throwable t) {
        hideProgressBar();
        if (mContext != null) {
            Log.d(TAG, t.toString());
            Toast.makeText(mContext, mContext.getResources().getString(R.string.error), Toast.LENGTH_SHORT).show();
        }
    }

    public void showProgressBar(Context context) {
        progressDialog = new ProgressDialog(context, R.style.MyTheme);
        progressDialog.setCancelable(true);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();
    }

    public void hideProgressBar() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public abstract void onRequestSuccess(T response);

}
