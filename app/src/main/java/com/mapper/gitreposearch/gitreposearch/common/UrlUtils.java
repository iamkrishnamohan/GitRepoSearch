package com.mapper.gitreposearch.gitreposearch.common;

/**
 * Created by user on 2/22/2018.
 */

public class UrlUtils {

    public static final String BASE_URL="https://api.github.com/";


    public static final String GET_USER_SEARCH_RESULTS=BASE_URL+"search/repositories";


    public static final String GET_CONTRIBUTORS=BASE_URL+"repos/";


}
