package com.mapper.gitreposearch.gitreposearch.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by user on 2/23/2018.
 */

public class ContributorBean implements Parcelable{

    private String contributorName;
    private String getContributorAvatar;
    private String contributorRepoUrl;

    public ContributorBean() {
    }

    public ContributorBean(Parcel in) {
        contributorName = in.readString();
        getContributorAvatar = in.readString();
        contributorRepoUrl = in.readString();
    }

    public static final Creator<ContributorBean> CREATOR = new Creator<ContributorBean>() {
        @Override
        public ContributorBean createFromParcel(Parcel in) {
            return new ContributorBean(in);
        }

        @Override
        public ContributorBean[] newArray(int size) {
            return new ContributorBean[size];
        }
    };

    public String getContributorRepoUrl() {
        return contributorRepoUrl;
    }

    public void setContributorRepoUrl(String contributorRepoUrl) {
        this.contributorRepoUrl = contributorRepoUrl;
    }

    public String getContributorName() {
        return contributorName;
    }

    public void setContributorName(String contributorName) {
        this.contributorName = contributorName;
    }

    public String getGetContributorAvatar() {
        return getContributorAvatar;
    }

    public void setGetContributorAvatar(String getContributorAvatar) {
        this.getContributorAvatar = getContributorAvatar;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(contributorName);
        parcel.writeString(getContributorAvatar);
        parcel.writeString(contributorRepoUrl);
    }
}
